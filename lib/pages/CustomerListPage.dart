import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:progress_indicator_button/progress_button.dart';

class CustomerListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Level Tracking',
      color: Colors.grey,
      theme: ThemeData(
        primarySwatch: Colors.green,
        scaffoldBackgroundColor: Colors.grey,
      ),
      home: CustomerListUI(),
    );
  }
}

class CustomerListUI extends StatefulWidget {
  @override
  CustomerList createState() => CustomerList();
}

class CustomerList extends State<CustomerListUI> {
  List<dynamic> products = []; // stores fetched products
  List<dynamic> productsBinary = []; // stores fetched products

  bool isLoading = false; // track if products fetching

  bool hasMore = true; // flag for more products available or not

  int documentLimit = 5; // documents to be fetched per request

  DocumentSnapshot
      lastDocument; // flag for last document from where next 10 records to be fetched

  ScrollController _scrollController = ScrollController();

  TextEditingController _searchValueTextController;
  String searchValue;

  @override
  void initState() {
    _searchValueTextController = TextEditingController();
    getProducts();

    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (maxScroll - currentScroll <= delta) {
        getProducts();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _searchValueTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Expanded(
          child: products.length == 0
              ? Center(
                  child: Text('No Data...'),
                )
              : ListView.builder(
                  controller: _scrollController,
                  itemCount: products.length,
                  itemBuilder: (context, index) {
                    return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        elevation: 5.0,
                        child: ListTile(
                          contentPadding: EdgeInsets.all(5),
                          title: Text(products[index].toString()),
                          subtitle: Text(productsBinary[index].toString()),
                          onTap: () {
                            print(
                                "SelectedValue ${products[index].toString()}");
                          },
                        ));
                  },
                ),
        ),
        isLoading
            ? Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(5),
                color: Colors.yellowAccent,
                child: Text(
                  'Loading',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : Container()
      ]),
    );
  }

  getProducts() async {
    if (!hasMore) {
      print('No More Products');
      return;
    }
    if (isLoading) {
      return;
    }
    setState(() {
      isLoading = true;
    });
    QuerySnapshot querySnapshot;

    print("documentLimit $documentLimit");

    if (lastDocument == null) {
      querySnapshot = await FirebaseFirestore.instance
          .collection('binaryDetails')
          .orderBy('sno', descending: false)
          .limit(documentLimit)
          .get();
    } else {
      querySnapshot = await FirebaseFirestore.instance
          .collection('binaryDetails')
          .orderBy('sno', descending: false)
          .startAfterDocument(lastDocument)
          .limit(documentLimit)
          .get();
    }
    if (querySnapshot.docs.length < documentLimit) {
      hasMore = false;
      setState(() {
        isLoading = false;
      });
    } else {
      lastDocument = querySnapshot.docs[querySnapshot.docs.length - 1];
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        products.add(querySnapshot.docs[i]['sno']);
        productsBinary.add(querySnapshot.docs[i]['payout']);
      }
      setState(() {
        isLoading = false;
      });
    }
  }
}
