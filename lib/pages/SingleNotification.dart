import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:progress_indicator_button/progress_button.dart';

class SingleNotification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Single Notification',
      color: Colors.grey,
      theme: ThemeData(
        primarySwatch: Colors.green,
        scaffoldBackgroundColor: Colors.grey,
      ),
      home: NotificationUI(),
    );
  }
}

class NotificationUI extends StatefulWidget {
  @override
  NotificationPage createState() => NotificationPage();
}

class NotificationPage extends State<NotificationUI> {
  TextEditingController titleController;
  TextEditingController messageController;
  TextEditingController _searchValueTextController;
  String searchValue;
  String appToken = "";

  @override
  void initState() {
    titleController = TextEditingController();
    messageController = TextEditingController();
    _searchValueTextController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    titleController.dispose();
    messageController.dispose();
    _searchValueTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey,
        alignment: Alignment.topCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 150.0,
                right: 50.0,
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                elevation: 16.0,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          bottom: 24.0,
                        ),
                        child: Text(
                          'Single Push Notification',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 22.0,
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: Container(
                          width: 400.0,
                          alignment: Alignment.center,
                          child: TextField(
                            keyboardType: TextInputType.text,
                            decoration: new InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.greenAccent, width: 2.0),
                              ),
                              hintText: 'Search with(UserID)',
                            ),
                            onChanged: (val) {
                              setState(() {
                                searchValue = val;
                              });
                            },
                            controller: _searchValueTextController,
                          ),
                        ),
                      ),
                      Container(
                        width: 400.0,
                        alignment: Alignment.center,
                        child: TextField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.green, width: 2.0),
                            ),
                            hintText: 'Notification Title',
                          ),
                          controller: titleController,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 24.0),
                        child: Container(
                          width: 400.0,
                          alignment: Alignment.center,
                          child: TextField(
                            decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              hintText: 'Notification Message',
                            ),
                            controller: messageController,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 24.0,
                        ),
                        child: Container(
                          width: 200.0,
                          height: 35.0,
                          child: ProgressButton(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                            strokeWidth: 2,
                            child: Text(
                              "Send Notification",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            color: Colors.green,
                            onPressed: (AnimationController controller) async {
                              if (titleController.text.toString().isEmpty ||
                                  messageController.text.toString().isEmpty) {
                                controller.reset();
                                showAlertDialog(context, "Error",
                                    "Enter all required data");
                              } else {
                                controller.forward();
                                FirebaseFirestore.instance
                                    .collection('userDetails')
                                    .where('userName', isEqualTo: searchValue)
                                    .snapshots()
                                    .listen(
                                  (data) {
                                    if (data.docs == null ||
                                        data.docs.length == 0) {
                                      controller.reset();
                                      showAlertDialog(
                                          context, "Error", "User Not Found");
                                    } else {
                                      setState(() {
                                        appToken = data.docs[0]['appToken'];
                                        if (appToken.isEmpty) {
                                          controller.reset();
                                          showAlertDialog(context, "Error",
                                              "Mobile App not registered by user");
                                        } else {
                                          controller.forward();
                                          sendPushNotification(
                                              context,
                                              titleController.text.toString(),
                                              messageController.text.toString(),
                                              controller,
                                              appToken,
                                              searchValue);
                                        }
                                      });
                                    }
                                  },
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> sendPushNotification(
    BuildContext context,
    String title,
    String message,
    AnimationController controller,
    String appToken,
    String customerID) async {
  var jsonReq = {
    "to": appToken,
    "notification": {"body": message, "title": title},
    "data": {"title": title, "message": message, "userName": customerID}
  };

  final response = await http.post(
    'https://fcm.googleapis.com/fcm/send',
    headers: {
      "Content-Type": "application/json",
      HttpHeaders.authorizationHeader:
          "key=AAAAdL90oWA:APA91bH2oxWW9rAUYPXpiHsYZrLm6od1IMm-Pf2WOHE72C_LL2R-vKxI4pTWVYz-e7T6se8E4uOhr2TqM6gKwatyfbPQNoqj_I4h9mfYzXJjmxAIMPDvMUotBniEsNGzgyBgc-T0mpnK"
    },
    body: json.encode(jsonReq),
  );

  final responseJson = jsonEncode(response.body);

  print("responseJson " + response.body);

  Map<String, dynamic> pushNotificationStatus = jsonDecode(response.body);

  if (pushNotificationStatus['message'].toString().isNotEmpty) {
    controller.reset();
    showAlertDialog(context, "Alert", "Notification Sent Successful");
  }
  return responseJson;
}

showAlertDialog(BuildContext context, String title, String message) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
