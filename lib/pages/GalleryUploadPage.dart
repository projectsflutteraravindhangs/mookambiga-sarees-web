import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'dart:html' as html;
import 'package:firebase/firebase.dart' as fb;
import 'package:progress_indicator_button/progress_button.dart';

class GalleryUploadPage extends StatefulWidget {
  @override
  GalleryUpload createState() => GalleryUpload();
}

class GalleryUpload extends State<GalleryUploadPage> {
  Future<void> _getImgFile(
      String imgName, AnimationController controller) async {
    html.File imageFile =
        await ImagePickerWeb.getImage(outputType: ImageType.file);
    setState(() {
      uploadImageFile(imageFile, imgName, controller).then((value) {
        print("value $value");
      });
    });
  }

  Future<Uri> uploadImageFile(
      html.File image, String imgName, AnimationController controller) async {
    fb.StorageReference storageRef =
        fb.storage().ref('Gallery/${DateTime.now().millisecondsSinceEpoch}');
    fb.UploadTaskSnapshot uploadTaskSnapshot =
        await storageRef.put(image).future;

    Uri imageUri = await uploadTaskSnapshot.ref.getDownloadURL();

    FirebaseFirestore.instance.collection('Gallery').add({
      'imageURL': imageUri.toString(),
      'time': FieldValue.serverTimestamp()
    }).then((value) async {
      print("value $value");
      controller.reset();
      showAlertDialog(context, "Success", "Images Uploaded Successful");
    });

    return imageUri;
  }

  TextEditingController imageTitleController;
  TextEditingController discountController;
  TextEditingController amountController;
  TextEditingController imageSubTitleController;

  @override
  void initState() {
    imageTitleController = TextEditingController();
    discountController = TextEditingController();
    amountController = TextEditingController();
    imageSubTitleController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    imageTitleController.dispose();
    discountController.dispose();
    amountController.dispose();
    imageSubTitleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey,
        alignment: Alignment.topCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 150.0,
                right: 50.0,
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                elevation: 16.0,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          bottom: 24.0,
                        ),
                        child: Text(
                          'Upload Gallery',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 22.0,
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: 400.0,
                        alignment: Alignment.center,
                        child: TextField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.green, width: 2.0),
                            ),
                            hintText: 'Image Name',
                          ),
                          controller: imageTitleController,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 24.0,
                        ),
                        child: Container(
                          width: 150.0,
                          height: 35.0,
                          child: ProgressButton(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                            strokeWidth: 2,
                            child: Text(
                              "Upload",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            color: Colors.green,
                            onPressed: (AnimationController controller) async {
                              if (imageTitleController.text
                                  .toString()
                                  .isEmpty) {
                                controller.reset();
                                showAlertDialog(
                                    context, "Error", "Enter required data");
                              } else {
                                controller.forward();
                                _getImgFile(
                                    imageTitleController.text.toString(),
                                    controller);
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, String title, String message) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
