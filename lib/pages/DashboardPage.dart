import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mookambigaweb/main.dart';
import 'package:mookambigaweb/pages/AddCustomer.dart';
import 'package:mookambigaweb/pages/CustomerListPage.dart';
import 'package:mookambigaweb/pages/GalleryEditPage.dart';
import 'package:mookambigaweb/pages/GalleryUploadPage.dart';
import 'package:mookambigaweb/pages/LevelTrackingPage.dart';
import 'package:mookambigaweb/pages/OffersEditPage.dart';
import 'package:mookambigaweb/pages/OffersUploadPage.dart';
import 'package:mookambigaweb/pages/responsive.dart';

import 'AllUserNotification.dart';
import 'SingleNotification.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery
        .of(context)
        .size * 0.80;
    return MaterialApp(
      title: 'Dashboard',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size(screenSize.width, 1000),
          child: Container(
            color: Theme
                .of(context)
                .bottomAppBarColor
                .withOpacity(0),
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                children: [
                  Container(
                    width: 50.0,
                    height: 50.0,
                    child: Image.asset('assets/applogo.png'),
                  ),
                  Text(
                    'Mookamiga Textiles',
                    style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                  Spacer(),
                  Container(
                    margin: EdgeInsets.only(right: 16.0),
                    child: InkWell(
                      onTap: () {
                        showGeneralDialog(
                          barrierLabel: "Barrier",
                          context: context,
                          barrierDismissible: true,
                          pageBuilder: (_, __, ___) => FunkyOverlay(),
                          barrierColor: Colors.black.withOpacity(0.5),
                          transitionDuration: Duration(milliseconds: 700),
                        );
                      },
                      child: Icon(
                        Icons.info_outline,
                        size: 28.0,
                        color: Colors.green,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showLogoutAlertDialog(context);
                    },
                    child: Icon(
                      Icons.power_settings_new_outlined,
                      size: 28.0,
                      color: Colors.green,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                color: Colors.white,
                height: screenSize.height * 0.6,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
              ),
              Center(
                heightFactor: 1,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: screenSize.height * 0.08,
                    left: ResponsiveWidget.isSmallScreen(context)
                        ? screenSize.width / 12
                        : screenSize.width / 5,
                    right: ResponsiveWidget.isSmallScreen(context)
                        ? screenSize.width / 12
                        : screenSize.width / 5,
                  ),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.account_circle_rounded,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Add Customer",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddCustomerPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.assessment_outlined,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Level Tracking",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          LevelTrackingPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.local_offer_outlined,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Upload Offers",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OffersUploadPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.notifications_none_rounded,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "All Notification",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SendNotification()),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.photo_album_outlined,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Upload Gallery",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          GalleryUploadPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.remove_circle_outline,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Edit Gallery",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => GalleryEditPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.edit,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Edit Offers",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OffersEditPage()),
                                );
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16.0),
                            child: InkWell(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Container(
                                  height: 140.0,
                                  width: 140.0,
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.notification_important,
                                        color: Colors.green,
                                        size: 52.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: 8.0,
                                        ),
                                        child: Text(
                                          "Single Notification",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10.0,
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SingleNotification()),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

showLogoutAlertDialog(BuildContext context) {
  // ignore: deprecated_member_use
  Widget yesButton = FlatButton(
    child: Text("Yes"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
      Navigator.pushAndRemoveUntil<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => MyApp(),
        ),
            (route) => false, //if you want to disable back feature set to false
      );
    },
  );

  // ignore: deprecated_member_use
  Widget noButton = FlatButton(
    child: Text("No"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  AlertDialog alert = AlertDialog(
    title: Text("Logout?"),
    content: Text("Are you sure. You want to logout?"),
    actions: [
      yesButton,
      noButton,
    ],
    elevation: 5.0,
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class BinaryDetailsUI extends StatefulWidget {
  @override
  _BinaryDetails createState() => _BinaryDetails();
}

class _BinaryDetails extends State<BinaryDetailsUI> {
  @override
  Widget build(BuildContext context) {
    return new StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('binaryDetails')
            .orderBy("sno", descending: false)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
              margin: EdgeInsets.only(
                top: 8.0,
                bottom: 8.0,
              ),
              child: Center(
                child: new Text(
                  "Fetching Details...",
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                  ),
                ),
              ),
            );
          }

          QuerySnapshot ds = snapshot.data;
          List<QueryDocumentSnapshot> documents = ds.docs;

          return SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: 8.0,
                      bottom: 8.0,
                    ),
                    child: Text(
                      "Binary Level Details",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Container(
                    child: DataTable(
                      headingRowColor: MaterialStateColor.resolveWith(
                              (states) => Colors.grey),
                      columns: [
                        DataColumn(
                          label: Text(
                            'No',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Level',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Members',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Payout',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                      rows: _buildList(context, documents),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  List<DataRow> _buildList(BuildContext context,
      List<DocumentSnapshot> snapshot) {
    return snapshot.map((data) => _buildListItem(context, data)).toList();
  }

  DataRow _buildListItem(BuildContext context, DocumentSnapshot data) {
    return DataRow(cells: [
      DataCell(
        Center(
          child: Text(
            data['sno'].toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      DataCell(
        Center(
          child: Text(
            data['level'].toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      DataCell(
        Center(
          child: Text(
            data['members'].toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      DataCell(
        Center(
          child: Text(
            data['payout'].toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    ]);
  }
}

class FunkyOverlay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FunkyOverlayState();
}

class FunkyOverlayState extends State<FunkyOverlay> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width * 0.30,
          decoration: ShapeDecoration(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0))),
          child: Container(
            child: BinaryDetailsUI(),
          ),
        ),
      ),
    );
  }
}
