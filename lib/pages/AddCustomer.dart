import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:progress_indicator_button/progress_button.dart';

TextEditingController customerIDController;
TextEditingController customerNameController;
TextEditingController customerMobileController;
TextEditingController customerAddressController;

class AddCustomerPage extends StatefulWidget {
  @override
  AddCustomer createState() => AddCustomer();
}

class AddCustomer extends State<AddCustomerPage> {
  String userName;

  @override
  void initState() {
    customerIDController = TextEditingController();
    customerNameController = TextEditingController();
    customerMobileController = TextEditingController();
    customerAddressController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    customerIDController.dispose();
    customerNameController.dispose();
    customerMobileController.dispose();
    customerAddressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Add Customer',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
          backgroundColor: Colors.grey,
          body: SingleChildScrollView(
            child: Container(
              color: Colors.grey,
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      top: 150.0,
                      right: 50.0,
                    ),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 16.0,
                      child: Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                bottom: 24.0,
                              ),
                              child: Text(
                                'Add Customer Details',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 22.0,
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              width: 400.0,
                              alignment: Alignment.center,
                              child: TextField(
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.green, width: 2.0),
                                  ),
                                  hintText: 'Customer ID',
                                ),
                                controller: customerIDController,
                                onChanged: (val) {
                                  setState(() {
                                    userName = val;
                                  });
                                },
                              ),
                            ),
                            Container(
                              width: 400.0,
                              margin: EdgeInsets.only(top: 24.0),
                              alignment: Alignment.center,
                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.green, width: 2.0),
                                  ),
                                  hintText: 'Customer Name',
                                ),
                                controller: customerNameController,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 24.0),
                              child: Container(
                                width: 400.0,
                                alignment: Alignment.center,
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  maxLength: 10,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.green, width: 2.0),
                                    ),
                                    hintText: 'Customer Mobile Number',
                                    counterText: "",
                                  ),
                                  controller: customerMobileController,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 24.0),
                              child: Container(
                                width: 400.0,
                                alignment: Alignment.center,
                                child: TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.green, width: 2.0),
                                    ),
                                    hintText: 'Customer Address',
                                  ),
                                  controller: customerAddressController,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                top: 24.0,
                              ),
                              child: Container(
                                width: 200.0,
                                height: 35.0,
                                child: ProgressButton(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3)),
                                    strokeWidth: 2,
                                    child: Text(
                                      "Add Customer",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    color: Colors.green,
                                    onPressed:
                                        (AnimationController controller) async {
                                      if (customerIDController.text
                                              .toString()
                                              .isEmpty ||
                                          customerNameController.text
                                              .toString()
                                              .isEmpty ||
                                          customerMobileController.text
                                              .toString()
                                              .isEmpty ||
                                          customerMobileController.text
                                                  .toString()
                                                  .length !=
                                              10 ||
                                          customerAddressController.text
                                              .toString()
                                              .isEmpty) {
                                        controller.reset();
                                        showAlertDialog(context, "Error",
                                            "Enter all required data");
                                      } else {
                                        controller.forward();
                                        setState(() {
                                          FirebaseFirestore.instance
                                              .collection('userDetails')
                                              .where("userName",
                                                  isEqualTo: userName)
                                              .get()
                                              .then((QuerySnapshot
                                                  querySnapshot) {
                                            if (querySnapshot.docs.isEmpty) {
                                              addCustomer(
                                                context,
                                                controller,
                                                customerAddressController.text
                                                    .toString(),
                                                customerNameController.text
                                                    .toString(),
                                                customerMobileController.text
                                                    .toString(),
                                                customerIDController.text
                                                    .toString(),
                                              );
                                            } else {
                                              controller.reset();
                                              showAlertDialog(context, "Error",
                                                  "Username already Exists");
                                            }
                                          });
                                        });
                                      }
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}

addCustomer(
    BuildContext context,
    AnimationController controller,
    String customerAddress,
    String customerName,
    String customerMobile,
    String customerID) {
  FirebaseFirestore.instance
      .collection('userDetails')
      .orderBy('seqNo', descending: true)
      .get()
      .then((data) {
    if (data.docs == null || data.docs.length == 0) {
      print("dataDocs No Data Found...");
    } else {
      print("data.docs ${data.docs[0]['seqNo']}");
      if (data.docs[0]['seqNo'] == -1) {
        int seqNo = 1;
        int userPosition = 1;
        int seqLevel = 0;
        int maxUserLevel = 1;
        int maxChild = 2;
        String myChild = "";
        int childs = 0;
        int parentSeqNo = 0;
        int totalChildren = 0;
        String isActive = "1";

        addCustomerField(
            context,
            controller,
            customerAddress,
            customerName,
            customerMobile,
            customerID,
            seqNo,
            userPosition,
            seqLevel,
            maxUserLevel,
            maxChild,
            myChild,
            childs,
            parentSeqNo,
            totalChildren,
            isActive);
        print("ifPrinted");
      } else {
        print("elsePrinted");
        int seqNo = data.docs[0]['seqNo'] + 1;
        int maxChild = 2; //Max no of child for parent
        String myChild = ""; //List of Children for this parent
        int childs = 0; //No of child presented

        int userPosition = 0; //User's position in the Current Level
        int seqLevel = 0; //Global seq Level
        int maxUserLevel = 0; //Max user in this level
        int parentSeqNo = 0; //Parent Seq no
        String parentMyChild = "";
        int parentChilds = 0;
        int totalChildren = 0;
        int totalParentChildren = 0;
        String isActive = "1";
        String isParentActive = "1";

        //Find and set User position and max user level
        print("maxLevel ${data.docs[0]['maxUserLevel']}");
        print("userPosition ${data.docs[0]['userPosition']}");

        if (data.docs[0]['maxUserLevel'] <= data.docs[0]['userPosition']) {
          seqLevel = data.docs[0]['seqLevel'] + 1;
          userPosition = 1;
          maxUserLevel = pow(2, seqLevel);

          print("powerValue seqLevel ${seqLevel}");
          print("powerValue maxUserLevel ${maxUserLevel}");
        } else {
          seqLevel = data.docs[0]['seqLevel'];
          userPosition = data.docs[0]['userPosition'] + 1;
          maxUserLevel = data.docs[0]['maxUserLevel'];
        }

        //Finding parent and updating seqNo
        print("userPosition ${userPosition}");

        if (data.docs[0]['maxUserLevel'] == 1) {
          parentSeqNo = 1;
        } else {
          if (data.docs[0]['userPosition'] % 2 == 0) {
            parentSeqNo = data.docs[0]['parentSeqNo'] + 1;
          } else {
            parentSeqNo = data.docs[0]['parentSeqNo'];
          }
        }

        //Find and Update Parent
        FirebaseFirestore.instance
            .collection('userDetails')
            .where('seqNo', isEqualTo: parentSeqNo)
            .get()
            .then((parentData) {
          print("MaxChild Parent ${parentData.docs[0]['maxChild']}");
          print("childs Parent ${parentData.docs[0]['childs']}");

          if (parentData.docs[0]['maxChild'] < parentData.docs[0]['childs']) {
            print("Failed to add user: Logic Error 1");
          } else {
            //update fetched data
            parentChilds = parentData.docs[0]['childs'] + 1;

            if (parentData.docs[0]['myChild'] == "") {
              parentMyChild = seqNo.toString();
            } else {
              parentMyChild =
                  parentData.docs[0]['myChild'] + "," + seqNo.toString();
            }

            if (parentData.docs[0]['totalChildren'] >= pow(2, 10) ||
                parentData.docs[0]['isActive'] != "1") {
              print("Failed to add user: Logic Error 2");
            } else {
              totalParentChildren = parentData.docs[0]['totalChildren'] + 1;
            }

            if (totalParentChildren == pow(2, 10)) {
              isParentActive = "0";
            }

            FirebaseFirestore.instance
                .collection("userDetails")
                .doc(parentData.docs[0].id)
                .update({
              'childs': parentChilds,
              'myChild': parentMyChild,
              'totalChildren': totalParentChildren,
              'isActive': isParentActive,
              'lastModifiedAt': FieldValue.serverTimestamp(),
              'lastModifiedFor': seqNo
            }).then((value) {
              print("Success: parentUpdated");
              addCustomerField(
                  context,
                  controller,
                  customerAddress,
                  customerName,
                  customerMobile,
                  customerID,
                  seqNo,
                  userPosition,
                  seqLevel,
                  maxUserLevel,
                  maxChild,
                  myChild,
                  childs,
                  parentSeqNo,
                  totalChildren,
                  isActive);
            });
          }
        }).catchError(
                (error) => print("Failed to add user: Logic Error $error"));
      }
    }
  });
}

addCustomerField(
    BuildContext context,
    AnimationController controller,
    String customerAddress,
    String customerName,
    String customerMobile,
    String customerID,
    int seqNo,
    int userPosition,
    int seqLevel,
    int maxUserLevel,
    int maxChild,
    String myChild,
    int childs,
    int parentSeqNo,
    int totalChildren,
    String isActive) {
  FirebaseFirestore.instance.collection('userDetails').add({
    'Address': customerAddress,
    'CustomerName': customerName,
    'mobileNumber': customerMobile,
    'isActive': isActive,
    'appToken': "",
    'userName': customerID,
    'password': "123456",
    'userCreatedAt': FieldValue.serverTimestamp(),
    'customerType': 'Binary',
    'seqNo': seqNo,
    'userPosition': userPosition,
    'seqLevel': seqLevel,
    'maxUserLevel': maxUserLevel,
    'maxChild': maxChild,
    'myChild': myChild,
    'childs': childs,
    "parentSeqNo": parentSeqNo,
    "totalChildren": totalChildren,
    "amountSent": 0
  }).then((value) async {
    controller.reset();
    customerIDController.text = "";
    customerNameController.text = "";
    customerMobileController.text = "";
    customerAddressController.text = "";

    showAlertDialog(context, "Success", "User Added Successful");
  }).catchError((error) => print("Failed to add user: $error"));
}

showAlertDialog(BuildContext context, String title, String message) {
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
