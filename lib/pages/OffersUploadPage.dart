import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'dart:html' as html;
import 'package:firebase/firebase.dart' as fb;
import 'package:progress_indicator_button/progress_button.dart';

class OffersUploadPage extends StatefulWidget {
  @override
  OffersUpload createState() => OffersUpload();
}

class OffersUpload extends State<OffersUploadPage> {
  Future<void> _getImgFile(String imgName, String discountStr, String amountStr,
      AnimationController controller, String imageSubTitle) async {
    html.File imageFile =
        await ImagePickerWeb.getImage(outputType: ImageType.file);
    setState(() {
      uploadImageFile(imageFile, imgName, discountStr, controller, amountStr,
              imageSubTitle)
          .then((value) {
        print("value $value");
      });
    });
  }

  Future<Uri> uploadImageFile(
      html.File image,
      String imgName,
      String discountStr,
      AnimationController controller,
      String amountStr,
      String imageSubTitle) async {
    fb.StorageReference storageRef =
        fb.storage().ref('Offers/${DateTime.now().millisecondsSinceEpoch}');
    fb.UploadTaskSnapshot uploadTaskSnapshot =
        await storageRef.put(image).future;

    Uri imageUri = await uploadTaskSnapshot.ref.getDownloadURL();

    FirebaseFirestore.instance.collection('Offers').add({
      'imageURL': imageUri.toString(),
      'imageName': imgName,
      'discount': discountStr,
      'amount': amountStr,
      'imageSubTitle': imageSubTitle,
      'time': FieldValue.serverTimestamp()
    }).then((value) async {
      print("value $value");
      controller.reset();
      showAlertDialog(context, "Success", "Images Uploaded Successful");
    });

    return imageUri;
  }

  TextEditingController imageTitleController;
  TextEditingController discountController;
  TextEditingController amountController;
  TextEditingController imageSubTitleController;

  @override
  void initState() {
    imageTitleController = TextEditingController();
    discountController = TextEditingController();
    amountController = TextEditingController();
    imageSubTitleController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    imageTitleController.dispose();
    discountController.dispose();
    amountController.dispose();
    imageSubTitleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey,
        alignment: Alignment.topCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 150.0,
                right: 50.0,
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                elevation: 16.0,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          bottom: 24.0,
                        ),
                        child: Text(
                          'Upload Offers',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 22.0,
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: 400.0,
                        alignment: Alignment.center,
                        child: TextField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.green, width: 2.0),
                            ),
                            hintText: 'Image Title',
                          ),
                          controller: imageTitleController,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 24.0,
                        ),
                        child: Container(
                          width: 400.0,
                          alignment: Alignment.center,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              hintText: 'Image Sub-Title',
                            ),
                            controller: imageSubTitleController,
                          ),
                        ),
                      ),
                      Container(
                        width: 400,
                        child: Padding(
                          padding: EdgeInsets.only(top: 24.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 197.0,
                                margin: EdgeInsets.only(
                                  right: 6.0,
                                ),
                                alignment: Alignment.center,
                                child: TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.green, width: 2.0),
                                    ),
                                    hintText: 'Amount',
                                  ),
                                  controller: amountController,
                                ),
                              ),
                              Container(
                                width: 197.0,
                                alignment: Alignment.center,
                                child: TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.green, width: 2.0),
                                    ),
                                    hintText: 'Discount',
                                  ),
                                  controller: discountController,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 24.0,
                        ),
                        child: Container(
                          width: 150.0,
                          height: 35.0,
                          child: ProgressButton(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                            strokeWidth: 2,
                            child: Text(
                              "Upload",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            color: Colors.green,
                            onPressed: (AnimationController controller) async {
                              if (imageTitleController.text
                                      .toString()
                                      .isEmpty ||
                                  discountController.text.toString().isEmpty ||
                                  amountController.text.toString().isEmpty ||
                                  imageSubTitleController.text
                                      .toString()
                                      .isEmpty) {
                                controller.reset();
                                showAlertDialog(context, "Error",
                                    "Enter all required data");
                              } else {
                                controller.forward();
                                _getImgFile(
                                    imageTitleController.text.toString(),
                                    discountController.text.toString(),
                                    amountController.text.toString(),
                                    controller,
                                    imageSubTitleController.text.toString());
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, String title, String message) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
