import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:firebase_storage/firebase_storage.dart';

class GalleryEditPage extends StatefulWidget {
  @override
  GalleryEdit createState() => GalleryEdit();
}

class GalleryEdit extends State<GalleryEditPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gallery Edit',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        backgroundColor: Colors.grey,
        body: FutureBuilder(
          // Initialize FlutterFire
          future: Firebase.initializeApp(),
          builder: (context, snapshot) {
            // Check for errors
            if (snapshot.hasError) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            // Once complete, show your application
            if (snapshot.connectionState == ConnectionState.done) {
              return new StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('Gallery')
                    .orderBy('time', descending: true)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  QuerySnapshot ds = snapshot.data;
                  List<QueryDocumentSnapshot> documents = ds.docs;

                  return StaggeredGridView.countBuilder(
                    crossAxisCount: 4,
                    itemCount: documents.length,
                    itemBuilder: (BuildContext context, int index) => Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 5.0,
                        margin: const EdgeInsets.all(10.0),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                height: 220,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        documents[index]["imageURL"]),
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.delete_forever,
                                        color: Colors.red,
                                        size: 32.0,
                                      ),
                                      onPressed: () {
                                        deleteOffer(
                                            context,
                                            documents[index]['imageURL'],
                                            documents[index].id);
                                      },
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        )),
                    staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                  );
                },
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  deleteOffer(BuildContext context, String imgUrl, String id) async {
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () async {
        Navigator.of(context, rootNavigator: true).pop('dialog');
        try {
          await fb.storage().refFromURL(imgUrl).delete();
          await FirebaseFirestore.instance
              .collection('Gallery')
              .doc(id)
              .delete()
              .then((value) {
            showAlertDialog(
                context, 'Offer Deleted', 'Offer Deleted Successful');
          });
        } catch (e) {
          print("Error deleting db from cloud: $e");
        }
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () async {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Delete?"),
      content: Text("Are you sure, you want to Delete?"),
      actions: [
        okButton,
        cancelButton
      ],
      elevation: 5.0,
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

timeConverter(DateTime timeStamp) {
  var outputFormat = DateFormat('dd/MM/yy hh:mm');
  var outputDate = outputFormat.format(timeStamp);
  print(outputDate);
  return outputDate;
}

showAlertDialog(BuildContext context, String title, String message) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

percentageCalc(String principalAmount, String percentage) {
  double disAmount = int.parse(principalAmount) * int.parse(percentage) / 100;
  double finalAmount = int.parse(principalAmount) - disAmount;

  return finalAmount.toStringAsFixed(2).toString();
}

saveAmountCalc(String principalAmount, String percentage) {
  double disAmount = int.parse(principalAmount) * int.parse(percentage) / 100;
  return disAmount.toStringAsFixed(2).toString();
}
