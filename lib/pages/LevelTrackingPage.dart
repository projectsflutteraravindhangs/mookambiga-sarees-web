import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphview/GraphView.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

Map<dynamic, dynamic> userNameList = new HashMap();
Map<dynamic, dynamic> finalUserDetails = new HashMap();
List<dynamic> sortedKeysVal = new List();
List<dynamic> nodeList = new List();

var clickedAgain = 0;

class LevelTrackingPage extends StatefulWidget {
  @override
  LevelTracking createState() => LevelTracking();
}

class LevelTracking extends State<LevelTrackingPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Level Tracking',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        backgroundColor: Colors.grey,
        body: TreeViewPage(),
      ),
    );
  }
}

class TreeViewPage extends StatefulWidget {
  @override
  _TreeViewPageState createState() => _TreeViewPageState();
}

class _TreeViewPageState extends State<TreeViewPage> {
  TextEditingController _searchValueTextController;
  TextEditingController _amountSentController;
  String searchValue = "";
  String customerID = "";
  String customerName = "";
  String customerMobileNo = "";
  String customerAddress = "";
  String customerCurrentLevel = "";
  String customerCurrentPayout = "";
  String customerAmountSent = "";
  bool isVisible = false;
  bool isGraphVisible = false;
  bool isAmountVisible = false;
  int amountSent = 0;
  String customerDBKeyID = "";

  Graph graph = Graph()..isTree = true;
  BuchheimWalkerConfiguration builder = BuchheimWalkerConfiguration();

  @override
  void initState() {
    _searchValueTextController = new TextEditingController();
    _amountSentController = new TextEditingController();
  }

  @override
  void dispose() {
    _searchValueTextController.dispose();
    _amountSentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pd = ProgressDialog(context: context);
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 8.0,
                    ),
                    child: Text(
                      "Customer Details",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.green,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        bottom: 16.0,
                        top: 16.0,
                        left: 16.0,
                      ),
                      child: Container(
                        width: 400.0,
                        alignment: Alignment.center,
                        child: TextField(
                          textInputAction: TextInputAction.go,
                          decoration: new InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.greenAccent, width: 2.0),
                            ),
                            hintText: 'Customer ID',
                            suffixIcon: IconButton(
                              onPressed: () {
                                if (_searchValueTextController.text
                                    .toString()
                                    .isEmpty) {
                                  showAlertDialog(context, "Error",
                                      "Please enter Username");
                                } else {
                                  setState(() {
                                    isVisible = false;
                                    isGraphVisible = false;
                                    isAmountVisible = false;
                                  });

                                  pd.show(
                                      max: 80,
                                      msg: "Searching....",
                                      progressValueColor: Colors.green,
                                      valuePosition: ValuePosition.center);

                                  userNameList.clear();
                                  finalUserDetails.clear();

                                  FirebaseFirestore.instance
                                      .collection('userDetails')
                                      .where("userName",
                                          isEqualTo: _searchValueTextController
                                              .text
                                              .toString())
                                      .get()
                                      .then((QuerySnapshot userDetails) {
                                    if (userDetails.docs.length == 0) {
                                      if (pd.isOpen()) {
                                        pd.close();
                                      }
                                      setState(() {
                                        _searchValueTextController.text = "";
                                      });
                                      showAlertDialog(
                                          context, "Error", "User not found");
                                    } else {
                                      if (userDetails.docs[0]['childs'] != 0 &&
                                          userDetails.docs[0]['childs'] != 1) {
                                        int localUserPosition =
                                            userDetails.docs[0]['seqNo'];

                                        customerDBKeyID =
                                            userDetails.docs[0].id;

                                        List<int> localPos = new List();
                                        int posVal = localUserPosition;
                                        int level = 10;

                                        for (var i = 0; i < level; i++) {
                                          posVal = posVal * 2;
                                          localPos.add(posVal);
                                        }

                                        FirebaseFirestore.instance
                                            .collection('userDetails')
                                            .where("seqNo", whereIn: localPos)
                                            .get()
                                            .then(
                                                (QuerySnapshot querySnapshot) {
                                          List<dynamic> seqNoDes = new List();

                                          for (int i = 0;
                                              i < querySnapshot.docs.length;
                                              i++) {
                                            seqNoDes.add(
                                                querySnapshot.docs[i]['seqNo']);
                                          }

                                          List<dynamic> ascending = new List();

                                          ascending = seqNoDes..sort();
                                          var descending = ascending.last;

                                          var userLevel =
                                              querySnapshot.docs.length;

                                          var userLevelLength = descending +
                                              pow(2, userLevel) -
                                              1;

                                          FirebaseFirestore.instance
                                              .collection('userDetails')
                                              .where("seqNo",
                                                  isEqualTo: userLevelLength)
                                              .get()
                                              .then((QuerySnapshot
                                                  userLevelFinding) {
                                            if (userLevelFinding.docs.length ==
                                                0) {
                                              userLevel = userLevel - 1;
                                            } else {
                                              userLevel = userLevel;
                                            }

                                            FirebaseFirestore.instance
                                                .collection('binaryDetails')
                                                .where("level",
                                                    isEqualTo:
                                                        userLevel.toString())
                                                .get()
                                                .then((QuerySnapshot
                                                    levelDetails) {
                                              int mapUserPosition =
                                                  userDetails.docs[0]['seqNo'];
                                              List<int> localMapPos =
                                                  new List();

                                              int posmapVal = mapUserPosition;
                                              int posmapVal1;
                                              int level = 3;
                                              localMapPos.add(
                                                  userDetails.docs[0]['seqNo']);
                                              for (var i = 1; i <= level; i++) {
                                                posmapVal = posmapVal * 2;
                                                localMapPos.add(posmapVal);
                                                posmapVal1 = posmapVal;
                                                for (var j = 0;
                                                    j < pow(2, i) - 1;
                                                    j++) {
                                                  posmapVal1 = posmapVal1 + 1;
                                                  localMapPos.add(posmapVal1);
                                                }
                                              }

                                              Map<dynamic, dynamic> firstList =
                                                  new HashMap();
                                              Map<dynamic, dynamic> secondList =
                                                  new HashMap();
                                              Map<dynamic, dynamic>
                                                  firstNodeList = new HashMap();
                                              Map<dynamic, dynamic>
                                                  secondNodeList =
                                                  new HashMap();

                                              firstList.clear();
                                              secondList.clear();
                                              firstNodeList.clear();
                                              secondNodeList.clear();

                                              int startIndex = 0;
                                              int endIndex = 10;
                                              List<dynamic> subList =
                                                  new List();
                                              subList = localMapPos.sublist(
                                                  startIndex, endIndex);

                                              int startNxtIndex = 10;
                                              int endNxtIndex =
                                                  localMapPos.length;
                                              List<dynamic> subNxtList =
                                                  new List();
                                              subNxtList = localMapPos.sublist(
                                                  startNxtIndex, endNxtIndex);

                                              FirebaseFirestore.instance
                                                  .collection('userDetails')
                                                  .where("seqNo",
                                                      whereIn: subList)
                                                  .get()
                                                  .then((QuerySnapshot
                                                      mapFinding) {
                                                for (var i = 0;
                                                    i < mapFinding.docs.length;
                                                    i++) {
                                                  firstList[mapFinding.docs[i]
                                                          ['seqNo']] =
                                                      mapFinding.docs[i]
                                                          ['userName'];
                                                  firstNodeList[i] = mapFinding
                                                      .docs[i]['userName'];
                                                }

                                                userNameList.addAll(firstList);

                                                finalUserDetails
                                                    .addAll(firstNodeList);

                                                FirebaseFirestore.instance
                                                    .collection('userDetails')
                                                    .where("seqNo",
                                                        whereIn: subNxtList)
                                                    .get()
                                                    .then((QuerySnapshot
                                                        finalMapFinding) {
                                                  for (var i = 0;
                                                      i <
                                                          finalMapFinding
                                                              .docs.length;
                                                      i++) {
                                                    secondList[finalMapFinding
                                                            .docs[i]['seqNo']] =
                                                        finalMapFinding.docs[i]
                                                            ['userName'];
                                                    secondNodeList[i] =
                                                        finalMapFinding.docs[i]
                                                            ['userName'];
                                                  }

                                                  userNameList
                                                      .addAll(secondList);
                                                  finalUserDetails
                                                      .addAll(secondList);

                                                  var sortedKeys = userNameList
                                                      .keys
                                                      .toList(growable: false)
                                                        ..sort((k1, k2) =>
                                                            userNameList[k1]
                                                                .compareTo(
                                                                    userNameList[
                                                                        k2]));
                                                  LinkedHashMap sortedMap =
                                                      new LinkedHashMap
                                                              .fromIterable(
                                                          sortedKeys,
                                                          key: (k) => k,
                                                          value: (k) =>
                                                              userNameList[k]);

                                                  var sortedKeysName =
                                                      userNameList.keys.toList()
                                                        ..sort();

                                                  sortedKeysVal.clear();
                                                  for (int i = 0;
                                                      i < sortedKeysName.length;
                                                      i++) {
                                                    sortedKeysVal
                                                        .add(sortedKeysName[i]);
                                                  }

                                                  finalUserDetails = sortedMap;
                                                  finalUserDetails.keys.toList()
                                                    ..sort();

                                                  if (pd.isOpen()) {
                                                    pd.close();
                                                  }
                                                  setState(() {
                                                    amountSent =
                                                    userDetails.docs[0]['amountSent'];
                                                    customerID = userDetails
                                                        .docs[0]['userName']
                                                        .toString();
                                                    customerName = userDetails
                                                        .docs[0]['CustomerName']
                                                        .toString();
                                                    customerMobileNo =
                                                        userDetails.docs[0]
                                                                ['mobileNumber']
                                                            .toString();
                                                    customerAddress =
                                                        userDetails.docs[0]
                                                            ['Address'];

                                                    customerCurrentLevel =
                                                        userLevel.toString();

                                                    if (userLevel == 0 ||
                                                        levelDetails
                                                                .docs.length ==
                                                            0) {
                                                      customerCurrentPayout =
                                                          "0";
                                                    } else {
                                                      customerCurrentPayout =
                                                          (levelDetails.docs[0]
                                                                  ['payout'])
                                                              .toString();
                                                    }

                                                    var lengthValue =
                                                        userNameList.length;
                                                    var i;
                                                    nodeList.clear();
                                                    for (i = 1;
                                                        i <= lengthValue / 2;
                                                        i++) {
                                                      nodeList.add(i);
                                                      nodeList.add(i);
                                                    }

                                                    if (lengthValue % 2 == 0) {
                                                      nodeList.removeLast();
                                                    }

                                                    graph = Graph()
                                                      ..isTree = true;
                                                    for (var i = 0;
                                                        i <=
                                                            nodeList.length - 1;
                                                        i++) {
                                                      graph.addEdge(
                                                          Node.Id(nodeList[i]),
                                                          Node.Id(i + 2));
                                                    }

                                                    builder
                                                      ..siblingSeparation = (50)
                                                      ..levelSeparation = (50)
                                                      ..subtreeSeparation = (50)
                                                      ..orientation =
                                                          (BuchheimWalkerConfiguration
                                                              .ORIENTATION_TOP_BOTTOM);
                                                    isVisible = true;
                                                    isGraphVisible = true;
                                                    isAmountVisible = true;
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      } else {
                                        if (pd.isOpen()) {
                                          pd.close();
                                        }
                                        setState(() {
                                          customerID = userDetails.docs[0]
                                                  ['userName']
                                              .toString();
                                          customerName = userDetails.docs[0]
                                                  ['CustomerName']
                                              .toString();
                                          customerMobileNo = userDetails.docs[0]
                                                  ['mobileNumber']
                                              .toString();
                                          customerAddress =
                                              userDetails.docs[0]['Address'];
                                          customerCurrentLevel = "0";
                                          customerCurrentPayout = "0";
                                          isVisible = true;
                                          isGraphVisible = false;
                                          isAmountVisible = false;
                                        });
                                      }
                                    }
                                  });
                                }
                              },
                              icon: Icon(Icons.search_rounded),
                            ),
                          ),
                          controller: _searchValueTextController,
                        ),
                      ),
                    ),
                    Visibility(
                      child: Padding(
                        padding: EdgeInsets.only(
                          bottom: 16.0,
                          top: 16.0,
                          right: 16.0,
                        ),
                        child: Container(
                          width: 400.0,
                          alignment: Alignment.center,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            decoration: new InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.greenAccent, width: 2.0),
                              ),
                              hintText: 'Amount Sent',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  if (_amountSentController.text
                                      .toString()
                                      .isEmpty) {
                                    showAlertDialog(context, "Error",
                                        "Please enter Username");
                                  } else {
                                    pd.show(max: 80, msg: "Updating...");
                                    FirebaseFirestore.instance
                                        .collection('userDetails')
                                        .doc(customerDBKeyID)
                                        .update({
                                      'amountSent': amountSent +  int.parse(_amountSentController.text
                                        .toString()),
                                    }).then((value) {
                                      if(pd.isOpen()){
                                        pd.close();
                                      }
                                      showAlertDialog(context, "Success", "Amount Updated successful");
                                      setState(() {
                                        amountSent = amountSent + int.parse(_amountSentController.text.toString());
                                        _amountSentController.text = "";
                                      });
                                    }).catchError((value) {
                                      if(pd.isOpen()){
                                        pd.close();
                                      }
                                      showAlertDialog(context, "Error", "$value");
                                    });
                                  }
                                },
                                icon: Icon(Icons.add),
                              ),
                            ),
                            controller: _amountSentController,
                          ),
                        ),
                      ),
                      visible: isAmountVisible,
                    ),
                  ],
                ),
              ],
            ),
            Visibility(
              child: Container(
                margin: EdgeInsets.only(
                  top: 16.0,
                  left: 16.0,
                ),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Table(
                          defaultColumnWidth: FixedColumnWidth(150.0),
                          children: [
                            TableRow(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    "Customer ID",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    customerID,
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    "Customer Name",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    customerName,
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    "Customer Mobile",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    "+91-" + customerMobileNo,
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    "Customer Address",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text(
                                    customerAddress,
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 10.0,
                            child: Container(
                              margin: EdgeInsets.all(16.0),
                              child: Column(
                                children: [
                                  Text(
                                    customerCurrentLevel,
                                    style: TextStyle(
                                      fontSize: 36.0,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "Current Level",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                        SizedBox(
                          width: 20.0,
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            left: 8.0,
                            right: 8.0,
                          ),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 10.0,
                            child: Container(
                              margin: EdgeInsets.all(16.0),
                              child: Column(
                                children: [
                                  Text(
                                    "₹ " + customerCurrentPayout,
                                    style: TextStyle(
                                      fontSize: 36.0,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "Amount Earned",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10.0,
                          child: Container(
                            margin: EdgeInsets.all(16.0),
                            child: Column(
                              children: [
                                Text(
                                  "₹ " + amountSent.toString(),
                                  style: TextStyle(
                                    fontSize: 36.0,
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "Amount Sent",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              visible: isVisible,
            ),
            Visibility(
              child: Container(
                child: Expanded(
                  child: InteractiveViewer(
                    constrained: false,
                    minScale: 0.01,
                    maxScale: 5.6,
                    scaleEnabled: false,
                    child: GraphView(
                      graph: graph,
                      algorithm: BuchheimWalkerAlgorithm(
                          builder, TreeEdgeRenderer(builder)),
                      paint: Paint()
                        ..color = Colors.green
                        ..strokeWidth = 1
                        ..style = PaintingStyle.stroke,
                      builder: (Node node) {
                        var a = node.key.value as int;
                        return rectangleWidget(a);
                      },
                    ),
                  ),
                ),
              ),
              visible: isGraphVisible,
            ),
          ],
        ),
      ),
    );
  }

  Widget rectangleWidget(int a) {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(color: Colors.green, spreadRadius: 1),
          ],
        ),
        child: Text(
          '${userNameList[sortedKeysVal[a - 1]]}',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, String title, String message) {
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop('dialog');
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
