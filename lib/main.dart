import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mookambigaweb/pages/DashboardPage.dart';
import 'package:mookambigaweb/pages/LevelTrackingPage.dart';
import 'package:progress_indicator_button/progress_button.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Admin Login',
      color: Colors.grey,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Admin Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController userNameController;
  TextEditingController passWordController;

  @override
  void initState() {
    // userNameController = TextEditingController(text: "admin@mookambiga.com");
    // passWordController = TextEditingController(text: "Admin@123");
    userNameController = TextEditingController();
    passWordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    userNameController.dispose();
    passWordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.bottomRight,
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    child: Image.asset('assets/applogo.png'),
                  ),
                  Text(
                    'Mookamiga Textiles',
                    style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: 150.0,
                    right: 50.0,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 16.0,
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                              bottom: 24.0,
                            ),
                            child: Text(
                              'Admin Login',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 22.0,
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            width: 400.0,
                            alignment: Alignment.center,
                            child: TextField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: new InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.green, width: 2.0),
                                ),
                                hintText: 'Username',
                              ),
                              controller: userNameController,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 24.0),
                            child: Container(
                              width: 400.0,
                              alignment: Alignment.center,
                              child: TextField(
                                obscureText: true,
                                obscuringCharacter: "*",
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.green, width: 2.0),
                                  ),
                                  hintText: 'Password',
                                ),
                                controller: passWordController,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 24.0,
                            ),
                            child: Container(
                              width: 150.0,
                              height: 35.0,
                              child: ProgressButton(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  strokeWidth: 2,
                                  child: Text(
                                    "Login",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  color: Colors.green,
                                  onPressed:
                                      (AnimationController controller) async {
                                    if (userNameController.text
                                            .toString()
                                            .isEmpty ||
                                        passWordController.text
                                            .toString()
                                            .isEmpty) {
                                      showAlertDialog(context, 'Error',
                                          'Enter all required data');
                                    } else {
                                      controller.forward();
                                      final FirebaseAuth auth =
                                          FirebaseAuth.instance;

                                      try {
                                        UserCredential userCredential =
                                            await FirebaseAuth.instance
                                                .signInWithEmailAndPassword(
                                                    email: userNameController
                                                        .text
                                                        .toString(),
                                                    password: passWordController
                                                        .text
                                                        .toString());

                                        final user = userCredential.user;

                                        final currentUser = auth.currentUser;

                                        if (user.uid == currentUser.uid) {
                                          controller.reset();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DashboardPage()),
                                          );
                                        }
                                      } on FirebaseAuthException catch (e) {
                                        if (e.code == 'user-not-found') {
                                          controller.reset();
                                          showAlertDialog(context, 'Error',
                                              'No User Found....');
                                        } else if (e.code == 'wrong-password') {
                                          controller.reset();
                                          showAlertDialog(context, 'Error',
                                              'Wrong password....');
                                        } else if (e.code == 'invalid-email') {
                                          controller.reset();
                                          showAlertDialog(context, 'Error',
                                              'Invalid Email....');
                                        }
                                      }
                                    }
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class LoadingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              child: CircularProgressIndicator(),
              height: 60.0,
              width: 60.0,
            ),
          ],
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, String title, String message) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: [
      okButton,
    ],
    elevation: 5.0,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
